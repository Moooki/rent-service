package com.accenture.russianatc.model.dto;

import com.accenture.russianatc.service.login.LoginSystem;
import org.springframework.beans.factory.annotation.Autowired;

public class LoginDTO {

    @Autowired
    private LoginSystem loginSystem;

    public LoginDTO() {
        System.out.println("Create new LoginDTO: " + this.hashCode());
    }

    /**
     * Проверяет введённые данные
     * @return Результат проверки
     */
    public boolean check(String login, String password, String confirmPassword) {
        return loginSystem.login(login, password, confirmPassword);
    }
}
