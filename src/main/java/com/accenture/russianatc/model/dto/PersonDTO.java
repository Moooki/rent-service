package com.accenture.russianatc.model.dto;

import com.accenture.russianatc.access.PersonDAO;
import com.accenture.russianatc.model.entity.user.Person;
import org.springframework.beans.factory.annotation.Autowired;

public class PersonDTO {

    @Autowired
    private PersonDAO personDAO;

    public PersonDTO() {
        System.out.println("Create new PersonDTO: " + this.hashCode());
    }

    /**
     * Собирает пользователя из пользователя (полученного из thymeleaf формы)
     * @param person Пользователь из thymeleaf формы
     * @return Пользователя собранного из базы данных
     */
    public Person setPerson(Person person) {
        return personDAO.setPerson(person);
    }
}
