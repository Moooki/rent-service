package com.accenture.russianatc.model.entity.user;

import com.accenture.russianatc.model.entity.rent.Rent;

import java.math.BigDecimal;

/**
 * Представляет арендатора
 */
public class User extends Person implements GroupAffiliation {

    private static final PersonGroup group = PersonGroup.User;

    private BigDecimal balance;
    private Rent rent;

    public User() {
        System.out.println("Create new User: " + this.hashCode());
    }

    public void setBalance(BigDecimal balance) { this.balance = balance; }
    public void setRent(Rent rent) { this.rent = rent; }

    public BigDecimal getBalance() { return balance; }
    public Rent getRent() { return rent; }

    @Override
    public PersonGroup getGroup() { return group; }

    @Override
    public String toString() {
        return "User" + "\n" +
                " ID: " + getId() + "\n" +
                " Login: " + getLogin() + "\n" +
                " Balance: " + balance + "\n" +
                " Rent: " + rent;
    }
}
