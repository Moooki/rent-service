package com.accenture.russianatc.model.entity.parking;

/**
 * Тип зоны парковки (Все, Только велосипеды, Только электросамокаты)
 */
public enum TypeParkingZone {
    All("All"), OnlyBike("OnlyBike"), OnlyElectricScooter("OnlyElectricScooter");

    private final String name;

    TypeParkingZone(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
