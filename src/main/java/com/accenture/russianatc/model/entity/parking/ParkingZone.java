package com.accenture.russianatc.model.entity.parking;

/**
 * Представляет зону парковки
 */
public class ParkingZone {
    private int id;
    private Point point;
    private double radius;
    private TypeParkingZone type;

    @Override
    public String toString() {
        return "ParkingZone " + "\n" +
                " ID: " + id + "\n" +
                " Point: " + point + "\n" +
                " Radius: " + radius + "\n" +
                " Type: " + type;
    }
}
