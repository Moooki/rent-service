package com.accenture.russianatc.model.entity.parking;

/**
 * Представляет координату (x, y)
 */
public class Point {
    private double x, y;

    @Override
    public String toString() {
        return "[X: " + x + " Y: " + y + "]";
    }
}
