package com.accenture.russianatc.model.entity.user;

public class Person {

    private long id;
    private String login;
    private String password;

    public Person() {
        System.out.println("Create new Person: " + this.hashCode());
    }

    public long getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
