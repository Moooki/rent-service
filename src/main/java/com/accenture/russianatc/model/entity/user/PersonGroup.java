package com.accenture.russianatc.model.entity.user;

public enum PersonGroup {
    User, Admin;
}
