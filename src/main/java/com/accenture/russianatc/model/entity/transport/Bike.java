package com.accenture.russianatc.model.entity.transport;

/**
 * Представляет велосипед
 */
public class Bike extends Vehicle {

    private static final VehicleType type = VehicleType.Bike;

    @Override
    public String toString() {
        return super.toString() + "Type: " + type + "\n";
    }
}