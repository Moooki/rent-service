package com.accenture.russianatc.model.entity.rent;

import com.accenture.russianatc.model.entity.parking.Point;
import com.accenture.russianatc.model.entity.transport.Vehicle;

/**
 * Представляет аренду транспортного средаства
 */
public class Rent {

    private long id;
    private Vehicle vehicle;
    private TimeRent sendingTime, arrivalTime;
    private Point sendingPoint, arrivalPoint;
    private StatusRent currentState;

    public void setId(long id) { this.id = id; }
    public void setVehicle(Vehicle vehicle) { this.vehicle = vehicle; }
    public void setSendingTime(TimeRent sendingTime) { this.sendingTime = sendingTime; }
    public void setArrivalTime(TimeRent arrivalTime) { this.arrivalTime = arrivalTime; }
    public void setSendingPoint(Point sendingPoint) { this.sendingPoint = sendingPoint; }
    public void setArrivalPoint(Point arrivalPoint) { this.arrivalPoint = arrivalPoint; }
    public void setCurrentState(StatusRent currentState) { this.currentState = currentState; }

    public long getId() { return id; }
    public Vehicle getVehicle() { return vehicle; }
    public TimeRent getSendingTime() { return sendingTime; }
    public TimeRent getArrivalTime() { return arrivalTime; }
    public Point getSendingPoint() { return sendingPoint; }
    public Point getArrivalPoint() { return arrivalPoint; }
    public StatusRent getCurrentState() { return currentState; }



    @Override
    public String toString() {
        return "Rent" + "\n" +
                " ID: " + id + "\n" +
                " Vehicle: " + vehicle + "\n" +
                " Send time: " + sendingTime + "\n" +
                " Arrival time: " + arrivalTime + "\n" +
                " Send point: " + sendingPoint + "\n" +
                " Arrival point: " + arrivalPoint + "\n" +
                " Status: " + currentState;
    }
}
