package com.accenture.russianatc.model.entity.transport;

import com.accenture.russianatc.model.entity.parking.ParkingZone;

/**
 * Абстрактное представление транспортного средства
 */
public abstract class Vehicle {
    private long id;
    private StatusVehicle status;
    private ParkingZone parkingZone;

    @Override
    public String toString() {
        return "Vehicle \n" +
                " ID: " + id + "\n" +
                " Status: " + status + "\n" +
                " Parking zone: " + parkingZone + "\n";
    }
}
