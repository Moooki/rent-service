package com.accenture.russianatc.model.entity.transport;

/**
 * Тип транспортного средства
 */
public enum VehicleType {
    Bike("Bike"), ElectricScooter("ElectricScooter");

    private final String name;

    VehicleType(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
