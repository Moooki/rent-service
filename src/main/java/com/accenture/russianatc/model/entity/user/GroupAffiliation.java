package com.accenture.russianatc.model.entity.user;

public interface GroupAffiliation {
    public PersonGroup getGroup();
}
