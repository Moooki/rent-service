package com.accenture.russianatc.model.entity.user;

/**
 * Представляет администратора
 */
public class Admin extends Person implements GroupAffiliation {

    private static final PersonGroup group = PersonGroup.Admin;

    public Admin() {
        System.out.println("Create new Admin: " + this.hashCode());
    }

    public void read(){}
    public void edit(){}
    public void add(){}
    public void delete(){}

    @Override
    public PersonGroup getGroup() { return group; }

    @Override
    public String toString() {
        return "Admin" + "\n" +
                " ID: " + getId() + "\n" +
                " Login: " + getLogin();
    }
}
